# `TcpSpeaker` 安卓上的网络`PCM`数据流播放器

因为我的新笔记本电脑声卡没有`linux`驱动程序，我不得已只得用手机做笔记本电脑的音箱，本来我用的是`soundwire`软件，后来我不太喜欢它，就自己编写了这个安卓播放器，可以直接播放`PCM`数据流，可以匹配`PulseAudio`的模块`module-simple-protocol-tcp`，经过优化，延迟很小，不比`soundwire`差。使用了前台服务技术，可以像系统的音乐播放器一样后台播放。具体`PCM`数据流的参数需求是：

- `format: s16le`
- `sampleRate: 20k`
- `channels: 1`